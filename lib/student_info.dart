import 'package:flutter/material.dart';

class StudentInfo extends StatelessWidget {
  const StudentInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          const Padding(padding: EdgeInsets.only(top: 25)),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //const Padding(padding: EdgeInsets.only(top: 10.0)),
              Container(
                height: 260,
                width: MediaQuery.of(context).size.width - 50,
                decoration: const BoxDecoration(
                  color: Color(0xFFBCAAA4),
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(1, 3),
                    )
                  ]
                ),
                padding: const EdgeInsets.all(15.0),
                child :Container(
                  //alignment: Alignment.center,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children:const <Widget>[
                            Text('ข้อมูลส่วนบุคคล',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),)
                          ],
                        ),
                        const Padding(padding: EdgeInsets.only(bottom:15.0)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                          Column(
                            children: [
                              Row(
                                children: const [
                                  CircleAvatar(
                                    radius: 55,
                                    backgroundImage: NetworkImage('imgs/profile2.jpg'),
                                  ),
                                ],
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: const [
                                  Text('ชื่อ - สกุล (อังกฤษ)',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: const [
                                  Text('BENYAPA BUNWISOOT',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),
                                  )
                                ],
                              ),
                              const Padding(padding: EdgeInsets.only(bottom:10)),
                              Row(
                                children: const [
                                  Text('ชื่อ - สกุล (ไทย)',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text('เบญญาภา บุญวิสูตร',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),
                                  )
                                ],
                              ),
                              const Padding(padding: EdgeInsets.only(bottom: 10)),
                              Row(
                                children: const [
                                  Text('วันเกิด',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text('21 ส.ค. 2544',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),
                                  )
                                ],
                              ),
                            ],
                          )
                        ]),
                      ],
                    ),
                ),
              ),
            ],
          ),
          const Padding(padding: EdgeInsets.only(top: 20)),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //const Padding(padding: EdgeInsets.only(top: 10.0)),
              Container(
                height: 275,
                // width: 350,
                width: MediaQuery.of(context).size.width - 50,
                decoration: const BoxDecoration(
                  color: Color(0xFFBCAAA4),
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(1, 3),
                    )
                  ]
                ),
                padding: const EdgeInsets.all(15.0),
                child :Container(
                  //alignment: Alignment.center,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children:const <Widget>[
                            Text('ข้อมูลการศึกษา',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),)
                          ],
                        ),
                        const Padding(padding: EdgeInsets.only(bottom:15.0)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: const [
                                  Text('รหัสประจำตัว:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: const [
                                  Text('63160285 ',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),
                                  )
                                ],
                              ),
                              const Padding(padding: EdgeInsets.only(bottom:10)),
                              Row(
                                children: const [
                                  Text('วิทยาเขต:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text('บางแสน',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),
                                  )
                                ],
                              ),
                              const Padding(padding: EdgeInsets.only(bottom:10)),
                              Row(
                                children: const [
                                  Text('หน่วยกิตที่ผ่าน:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text('91',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),
                                  )
                                ],
                              ),
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(bottom:15.0)),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: const [
                                  Text('คณะ:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: const [
                                  Text('คณะวิทยาการสารสนเทศ',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),
                                  )
                                ],
                              ),
                              const Padding(padding: EdgeInsets.only(bottom:10)),
                              Row(
                                children: const [
                                  Text('ระดับการศึกษา:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text('ปริญญาตรี',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),
                                  )
                                ],
                              ),
                              const Padding(padding: EdgeInsets.only(bottom:10)),
                              Row(
                                children: const [
                                  Text('คะแนนเฉลี่ยสะสม:',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: const [
                                  Text('3.42',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),
                                  )
                                ],
                              ),
                              const Padding(padding: EdgeInsets.only(bottom:10)),
                            ],
                          )
                        ]
                        ),
                        // const Padding(padding: EdgeInsets.only(bottom:15.0)),
                        // Row(
                        //   children: [
                        //   Column(
                        //     crossAxisAlignment: CrossAxisAlignment.start,
                        //     children: [
                        //       Row(
                        //         children: const [
                        //           Text('อาจารย์ที่ปรึกษา',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),
                        //           ),
                        //         ],
                        //       ),
                        //       const Padding(padding: EdgeInsets.only(bottom:10)),
                        //        Row(
                        //         children: const [
                        //           Text('อาจารย์ภูสิต กุลเกษม',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),                               
                        //           ),
                        //         ],
                        //       ),
                        //       Row(
                        //         children: const [
                        //           Text('ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w100),
                        //           ),
                        //         ],
                        //       ),
                        //     ]
                        // )]
                        // )
                      ],
                    ),
                ),
              ),
            ],
          ),
          const Padding(padding: EdgeInsets.only(top: 20)),
        ]
      ),
    );
  }
}
