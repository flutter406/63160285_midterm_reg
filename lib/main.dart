import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:midterm_reg/student_info.dart';
import 'package:midterm_reg/studies_info.dart';
import 'studies_table.dart';

void main() {
  runApp(CreateRegSystem());
}
class CreateRegSystem extends StatefulWidget {
  @override
  State<CreateRegSystem> createState() => _CreateRegSystem();
}

class _CreateRegSystem extends State<CreateRegSystem> {
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'ระบบลงทะเบียนมหาวิทยาลัยบูรพา',
        home: const HomePage(),
      )
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super (key:key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          backgroundColor: Colors.brown[50],
          appBar: AppBar(
          backgroundColor: Colors.brown[200],
          title: const Text(
            "ยินดีต้อนรับ",
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 28, letterSpacing: 1.5 ),
            ),
          ),
          drawer: SideNav(),
          body: const MainPage()
        );
  }
}
class SideNav extends StatefulWidget {
  @override
  State<SideNav> createState() => _SideNav();
}

class _SideNav extends State<SideNav> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.brown.shade100,
      child: ListView(
        children: <Widget>[
          buildHeader(context),
          buildItems(context),
        ],
      ),
    );
  }
  Widget buildHeader(BuildContext context) {
    return Container(
      color: Colors.brown.shade200,
      padding: const EdgeInsets.only(top :20,bottom: 20),
      child: Column(
        children: const [
          CircleAvatar(
            radius: 55,
            backgroundImage: NetworkImage('imgs/profile2.jpg'),
          ),
          SizedBox(height: 10),
          Text('เบญญาภา บุญวิสูตร', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Text('63160285@go.buu.ac.th',style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }

  Widget buildItems(BuildContext context) {
    return  Column(
      children: [
        ListTile(
            title: const Text("หน้าหลัก", style: TextStyle(fontSize: 24),),
            leading: const Icon(Icons.home_outlined),
            onTap: () => 
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const HomePage()))
          ),
      //  ListTile(
      //       title: const Text("ผลการลงทะเบียน", style: TextStyle(fontSize: 24),),
      //       leading: const Icon(Icons.article_outlined),
      //       onTap: () => 
      //         Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Register()))
      //     ),
          // ListTile(
          //   title: const Text("ตารางเรียน / สอบ", style: TextStyle(fontSize: 24),),
          //   leading: const Icon(Icons.table_chart_outlined),
          //   onTap: () => 
          //     Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Schedule()))
          // ),
          ListTile(
            title: const Text("ประวัตินิสิต", style: TextStyle(fontSize: 24),),
            leading: const Icon(Icons.people_alt_outlined),
            onTap: () => 
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const Information()))
          ),
          // ListTile(
          //   title: const Text("ตารางการใช้ห้อง", style: TextStyle(fontSize: 24),),
          //   leading: const Icon(Icons.table_chart_outlined),
          //   onTap: () => 
          //     Navigator.of(context).push(MaterialPageRoute(builder: (context) => const RoomSchedule()))
          // ),
          ListTile(
            title: const Text("ผลการศึกษา", style: TextStyle(fontSize: 24),),
            leading: const Icon(Icons.calendar_month_outlined),
            onTap: () => 
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => const StudyResult()))
          ),
          ListTile(
            title: const Text("ออกจากระบบ", style: TextStyle(fontSize: 24),),
            leading: const Icon(Icons.logout_outlined),
            onTap: () => 
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Logout()))
          )
      ],
    );      
  }
}

//Routing part
class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        const Padding(padding: EdgeInsets.only(top: 25)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            //const Padding(padding: EdgeInsets.only(top: 10.0)),
            Container(
              child: const Image(image: NetworkImage('imgs/buu.png'),height: 150,width: 150,),
            ),
          ],
        ),
        const Padding(padding: EdgeInsets.only(top:20)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //const Padding(padding: EdgeInsets.only(top: 10.0)),
            Container(
              height: 100,
              width: MediaQuery.of(context).size.width-20,
              decoration: const BoxDecoration(
                color: Color(0xFFBCAAA4),
                borderRadius: BorderRadius.all(Radius.circular(20)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(1, 3),
                  )
                ]
              ),
              padding: const EdgeInsets.all(15.0),
              child :Container(
                alignment: Alignment.center,
                child: const Text('ข่าวสารประชาสัมพันธ์',
                style: TextStyle(fontSize: 26,color: Colors.white, fontWeight: FontWeight.w500),),
              ),
            ),
          ],
        ),
        const Padding(padding: EdgeInsets.only(top:25)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //const Padding(padding: EdgeInsets.only(top: 10.0)),
            Container(
              height: 100,
              width: MediaQuery.of(context).size.width-20,
              decoration: const BoxDecoration(
                color: Color(0xFFBCAAA4),
                borderRadius: BorderRadius.all(Radius.circular(20)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(1, 3),
                  )
                ]
              ),
              padding: const EdgeInsets.all(15.0),
              child :Container(
                alignment: Alignment.center,
                child: const Text('ลงทะเบียน',
                style: TextStyle(fontSize: 26,color: Colors.white, fontWeight: FontWeight.w500),),
              ),
            ),
          ],
        ),
        const Padding(padding: EdgeInsets.only(top:25)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //const Padding(padding: EdgeInsets.only(top: 10.0)),
            Container(
              height: 100,
              width: MediaQuery.of(context).size.width-20,
              decoration: const BoxDecoration(
                color: Color(0xFFBCAAA4),
                borderRadius: BorderRadius.all(Radius.circular(20)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(1, 3),
                  )
                ]
              ),
              padding: const EdgeInsets.all(15.0),
              child :Container(
                alignment: Alignment.center,
                child: const Text('ผลการอนุมัติลงทะเบียน',
                style: TextStyle(fontSize: 26,color: Colors.white, fontWeight: FontWeight.w500),),
              ),
            ),
          ],
        ),
        const Padding(padding: EdgeInsets.only(top:25)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //const Padding(padding: EdgeInsets.only(top: 10.0)),
            Container(
              height: 100,
              width: MediaQuery.of(context).size.width-20,
              decoration: const BoxDecoration(
                color: Color(0xFFBCAAA4),
                borderRadius: BorderRadius.all(Radius.circular(20)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(1, 3),
                  )
                ]
              ),
              padding: const EdgeInsets.all(15.0),
              child :Container(
                alignment: Alignment.center,
                child: const Text('ยื่นคำร้อง',
                style: TextStyle(fontSize: 26,color: Colors.white, fontWeight: FontWeight.w500),),
              ),
            ),
          ],
        ),
        const Padding(padding: EdgeInsets.only(top:25)),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   children: <Widget>[
        //     //const Padding(padding: EdgeInsets.only(top: 10.0)),
        //     Container(
        //       height: 100,
        //       width: 350,
        //       decoration: const BoxDecoration(
        //         color: Color(0xFFBCAAA4),
        //         borderRadius: BorderRadius.all(Radius.circular(20)),
        //         boxShadow: [
        //           BoxShadow(
        //             color: Colors.black12,
        //             spreadRadius: 5,
        //             blurRadius: 7,
        //             offset: Offset(1, 3),
        //           )
        //         ]
        //       ),
        //       padding: const EdgeInsets.all(15.0),
        //       child :Container(
        //         alignment: Alignment.center,
        //         child: const Text('ยื่นคำร้อง',
        //         style: TextStyle(fontSize: 26,color: Colors.white, fontWeight: FontWeight.w500),),
        //       ),
        //     ),
        //   ],
        // ),
      ],
    );
  }
}
// class Register extends StatelessWidget {
//   const Register({Key? key}) : super(key:key);
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//           backgroundColor: Colors.brown[50],
//           appBar: AppBar(
//           backgroundColor: Colors.brown[200],
//           title: const Text(
//             "ผลการลงทะเบียน",
//             style: TextStyle(fontWeight: FontWeight.w600, fontSize: 25, letterSpacing: 1.5 ),
//             ),
//           ),
//         );
//   }
// }

// class Schedule extends StatelessWidget {
//   const Schedule({Key? key}) : super(key:key);
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//           backgroundColor: Colors.brown[50],
//           appBar: AppBar(
//           backgroundColor: Colors.brown[200],
//           title: const Text(
//             "ตารางเรียน / สอบ",
//             style: TextStyle(fontWeight: FontWeight.w600, fontSize: 25, letterSpacing: 1.5 ),
//             ),
//           ),
//         );
//   }
// }

class Information extends StatelessWidget {
  const Information({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          backgroundColor: Colors.brown[50],
          appBar: AppBar(
          backgroundColor: Colors.brown[200],
          title: const Text(
            "ประวัตินิสิต",
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 25, letterSpacing: 1.5 ),
            ),
          ),
          body: ListView(
            children: [
              ConstrainedBox(constraints: const BoxConstraints(maxWidth: 500),
              child: Center(
                child: Column(
                  children: const <Widget>[
                    Padding(padding: EdgeInsets.only(top: 20.0)),
                    StudentInfo(),
                  ]),
                ) 
              ),
            ],
          )
        );
  }
}

// class RoomSchedule extends StatelessWidget {
//   const RoomSchedule({Key? key}) : super(key:key);
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//           backgroundColor: Colors.brown[50],
//           appBar: AppBar(
//           backgroundColor: Colors.brown[200],
//           title: const Text(
//             "ตารางการใช้ห้อง",
//             style: TextStyle(fontWeight: FontWeight.w600, fontSize: 25, letterSpacing: 1.5 ),
//             ),
//           ),
//         );
//   }
// }

class StudyResult extends StatelessWidget {
  const StudyResult({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          backgroundColor: Colors.brown[50],
          appBar: AppBar(
          backgroundColor: Colors.brown[200],
          title: const Text(
            "ผลการศึกษา",
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 25, letterSpacing: 1.5 ),
            ),
          ),
          body: ListView(
            children: [
              ConstrainedBox(constraints: const BoxConstraints(maxWidth: 500),
              child: Center(
                child: Column(
                  children: const <Widget>[
                    Padding(padding: EdgeInsets.only(top: 20.0)),
                    StudiesInfo(),
                    Padding(padding: EdgeInsets.only(bottom: 20.0)),
                    StudiesTable()
                  ]),
                ) 
              ),
            ],
          )
        );
  }
}

class Logout extends StatelessWidget {
  const Logout({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          backgroundColor: Colors.brown[50],
          appBar: AppBar(
          backgroundColor: Colors.brown[200],
          title: const Text(
            "เข้าสู่ระบบ",
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 25, letterSpacing: 1.5 ),
            ),
          ),
          body: Center(
            child: ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 300.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
            // const SizedBox(height: 50.0),
                  const Spacer(
                    flex: 5,
                  ),
            //const FlutterLogo(size: 100.0),
                  const Expanded(
                    flex: 10,
                    child:Image(image: NetworkImage('imgs/buu.png'))
                  ),
                  const Spacer(
                    flex: 5,
                  ),
                  const Text('มหาวิทยาลัยบูรพา',style: TextStyle(fontSize: 26,fontWeight: FontWeight.w500),),
                  // const SizedBox(height: 100.0),
                  const Spacer(
                    flex: 5,
                  ),
                  const TextField(decoration: InputDecoration(labelText: 'รหัสประจำตัว')),
                  const TextField(decoration: InputDecoration(labelText: 'รหัสผ่าน')),
                  const Spacer(flex: 3),
                  // const SizedBox(height: 30.0),
                  ElevatedButton(onPressed: () {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const HomePage()));
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.brown.shade200,
                      padding: const EdgeInsets.only(left: 20,right: 20,top: 10,bottom: 10),
                    ), 
                    child: const Text('เข้าสู่ระบบ',style: TextStyle(fontSize: 22,fontWeight: FontWeight.w300),),
                    ),
                  const Spacer(
                    flex: 20,
                  ),
                ],
              ),
            ),
          )
    );
  }
}