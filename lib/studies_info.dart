import 'package:flutter/material.dart';

class StudiesInfo extends StatelessWidget {
  const StudiesInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: const [
          CircleAvatar(
            radius: 55,
            backgroundImage: NetworkImage('imgs/profile2.jpg'),
          ),
          SizedBox(height: 10),
          Text('เบญญาภา บุญวิสูตร', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          // Text('ภาคการศึกษา 1/2565',style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          DropdownResult(),
        ],
      ),
    );
  }
}


class DropdownResult extends StatefulWidget{
  const DropdownResult({Key? key}) : super(key: key);
  @override
  _Dropdownres createState() => _Dropdownres();
}

class _Dropdownres extends State<DropdownResult> {
  String dropdownvalue = 'ภาคการศึกษา 1/2565';

  var items = [
    'ภาคการศึกษา 1/2563',
    'ภาคการศึกษา 2/2563',
    'ภาคการศึกษา 1/2564',
    'ภาคการศึกษา 2/2564',
    'ภาคการศึกษา 1/2565',
    'ภาคการศึกษา 2/2565',
  ];

  @override
  Widget build(BuildContext context) {
    return DropdownButton(value: dropdownvalue,
      icon: const Icon(Icons.keyboard_arrow_down),
      items: items.map((String items) {
        return DropdownMenuItem(
          value: items,
          child: Text(items),
        );
      }).toList(),
      onChanged: (String? newValue) { 
        setState(() {
          dropdownvalue = newValue!;
        });
      },
    );
  }
}