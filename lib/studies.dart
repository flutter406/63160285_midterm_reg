class Studies {
  final String courseid;
  final String subject;
  final int credit;
  final String grade;

  Studies(this.courseid, this.subject, this.credit, this.grade);
  static List<Studies> getStudies() {
    return [
      Studies('68432959', 'Integrative Antiaging', 2,'A'),
      Studies('88624259', 'Mobile Programming Paradigm', 3,'B+'),
      Studies('88631159', 'Algorithm Design and Applications', 3,'B'),
      Studies('88633159', '	Computer Networks', 3,'B'),
      Studies('88634159', 'Software Development', 3,'A'),
      Studies('88635359', 'User Interface Design and Development', 3,'B+'),
      Studies('88636159', '	Introduction to Artificial Intelligence', 3,'A'),
    ];
  }
}