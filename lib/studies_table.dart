import 'package:flutter/material.dart';

import 'studies_cell.dart';
import 'studies.dart';

class StudiesTable extends StatelessWidget {
  const StudiesTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width-30;
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth:width),
      child: Table(
      columnWidths : const <int, TableColumnWidth> {
        0: FixedColumnWidth(100)
      },
      defaultColumnWidth : const FlexColumnWidth(),
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          decoration: BoxDecoration(
            color: Colors.brown[200],
          ),
          children: const <Widget>[
            StudiesCell(title: 'Course ID', isHeader: true),
            StudiesCell(title: 'Subject', isHeader: true),
            StudiesCell(title: 'Credit', isHeader: true),
            StudiesCell(title: 'Grade', isHeader: true),
          ],
        ),
        ...Studies.getStudies().map((studies) {
          return TableRow(children: [
            StudiesCell(title: studies.courseid),
            StudiesCell(title: studies.subject),
            StudiesCell(title: (studies.credit).toString()),
            StudiesCell(title: studies.grade),
          ]);
        }),
      ],
    ) ,
    );
  }
}